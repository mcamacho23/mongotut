import datetime
from flask import Flask, render_template,request,redirect,url_for # For flask implementation
from pymongo import MongoClient # Database connector
from bson.objectid import ObjectId # For ObjectId to work

client = MongoClient("mongodb+srv://miguel:mbit2018@cluster0-ril4a.mongodb.net/test")
# client = MongoClient('localhost', 27017)        #Configure the connection to the database
db = client.mydatabase                          #Select the database
mycollection = db.mycollection                  #Select the collection

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/addpost')
def addpost():
    return render_template('addpost.html')

@app.route('/list')
def lists():
    docs = mycollection.find()
    authors = mycollection.distinct("by")
    return render_template('index.html', docs=docs, authors=authors)

@app.route("/create", methods=['POST'])
def create():
    title = request.values.get("title")
    desc = request.values.get("desc")
    by = request.values.get("by")
    mycollection.insert({"title": title, "desc": desc, "by": by, "like": 0, "helpful": 0, "created": datetime.datetime.now()})
    return redirect("/list")

@app.route("/remove")
def remove():
    id = request.values.get("_id")
    mycollection.remove({"_id":ObjectId(id)})
    return redirect("/list")

@app.route("/update")
def update():
    id = request.values.get("_id")
    docs = mycollection.find({"_id": ObjectId(id)})
    return render_template('editpost.html', docs=docs)

@app.route("/update", methods=['POST'])
def update_post():
    title = request.values.get("title")
    desc = request.values.get("desc")
    by = request.values.get("by")
    id = request.values.get("_id")
    mycollection.update({"_id":ObjectId(id)}, {'$set':{ "title":title, "desc":desc, "by":by }})
    return redirect("/list")

@app.route("/author")
def author():
    by = request.values.get("by")
    docs = mycollection.find({"by": by})
    return render_template('author.html', docs=docs, by=by)

@app.route("/remove_user")
def remove_user():
    by = request.values.get("by")
    mycollection.remove({"by": by})
    return redirect("/list")

@app.route("/get_post")
def get_post():
    id = request.values.get("_id")
    docs = mycollection.find({"_id": ObjectId(id)})
    return render_template('getpost.html', docs=docs, id=id)

@app.route("/add_comment", methods=['POST'])
def add_comment():
    id = request.values.get("_id")
    comment = request.values.get("comment")
    mycollection.update({"_id": ObjectId(id)}, {'$push': {'comments':comment}})
    return redirect("/get_post?_id=" + id)

if __name__ == '__main__':
    app.run()